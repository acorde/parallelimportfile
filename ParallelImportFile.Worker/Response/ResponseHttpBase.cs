﻿namespace ParallelImportFile.worker.Response
{
    public abstract class ResponseHttpBase
    {
        public int? success { get; set; }
        public string? resultMessage { get; set; }
    }
}
