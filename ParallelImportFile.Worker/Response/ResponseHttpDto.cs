﻿namespace ParallelImportFile.worker.Response
{
    public class ResponseHttpDto : ResponseHttpBase
    {
        public string? type { get; set; }
        public string? title { get; set; }
        public int? status { get; set; }
        public string? traceId { get; set; }
    }
}
