//using Newtonsoft.Json;
using OfficeOpenXml;
using ParallelImportFile.worker.Entity;
using ParallelImportFile.worker.Response;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ParallelImportFile.Worker;

public class Worker : BackgroundService
{
    private readonly ILogger<Worker> _logger;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly JsonSerializerOptions _options;

    public Worker(ILogger<Worker> logger
        , IHttpClientFactory httpClientFactory)
    {
        _logger = logger;
        _httpClientFactory = httpClientFactory;
        _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            if (_logger.IsEnabled(LogLevel.Information))
            {
                //_logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                var itau = new Action(LerArquivoItau);
                //var bradesco = new Action(LerArquivoBradesco);
                //var santander = new Action(LerArquivoSantander);
                //var nubankCsv = new Action(LerArquivoNubank);
                //var multiposChannels = new Action(LerArquivoParaMultiplosChannels);

                var stopWatch = new Stopwatch();
                stopWatch.Start();
                Parallel.Invoke(new ParallelOptions { MaxDegreeOfParallelism = 1 }, itau);
                //Parallel.Invoke(new ParallelOptions { MaxDegreeOfParallelism = 4 }, itau, bradesco, santander, nubankCsv);
                stopWatch.Stop();
                Console.WriteLine($"O Tempo total de execu��o � {stopWatch.Elapsed.TotalMilliseconds} ms");
                Thread.Sleep(1500);
            }
            await Task.Delay(60000, stoppingToken);
        }
    }

    private async Task SendParaMultiplosChannels(Customers customer)
    {

        try
        {
            var json = JsonSerializer.Serialize(customer);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var url = "https://localhost:7098/api/Customers/MultiplosChannels";
            using var _httpClient = new HttpClient();
            using HttpResponseMessage httpResponse = await _httpClient.PostAsync(url, data);
            var result = await httpResponse.Content.ReadAsStringAsync();
            if (httpResponse.IsSuccessStatusCode)
                _logger.LogInformation($"{httpResponse.IsSuccessStatusCode}");
            else
                _logger.LogError($"{httpResponse.IsSuccessStatusCode}");

        }
        catch (Exception ex)
        {
            _logger.LogError($"Ocorreu o seguinte erro: {ex.Message}");
        }
    }

    private async Task SendCustomersNubank(Customers customer)
    {

        try
        {
            var json = JsonSerializer.Serialize(customer);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var url = "https://localhost:7098/api/Customers/Nubank";
            using var _httpClient = new HttpClient();
            using HttpResponseMessage httpResponse = await _httpClient.PostAsync(url, data);
            //var result = await httpResponse.Content.ReadAsStringAsync();
            if (httpResponse.IsSuccessStatusCode)
                _logger.LogInformation($"{httpResponse.IsSuccessStatusCode}");
            else
                _logger.LogError($"{httpResponse.IsSuccessStatusCode}");

        }
        catch (Exception ex)
        {
            _logger.LogError($"Ocorreu o seguinte erro: {ex.Message}");
        }
    }

    private async Task SendCustomersSantander(Customers customer)
    {

        try
        {
            var json = JsonSerializer.Serialize(customer);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var url = "https://localhost:7098/api/Customers/Santander";
            using var _httpClient = new HttpClient();
            using HttpResponseMessage httpResponse = await _httpClient.PostAsync(url, data);
            //var result = await httpResponse.Content.ReadAsStringAsync();
            if (httpResponse.IsSuccessStatusCode)
                _logger.LogInformation($"{httpResponse.IsSuccessStatusCode}");
            else
                _logger.LogError($"{httpResponse.IsSuccessStatusCode}");

        }
        catch (Exception ex)
        {
            _logger.LogError($"Ocorreu o seguinte erro: {ex.Message}");
        }
    }

    private async Task SendCustomersBradesco(Customers customer)
    {

        try
        {
            var _httpClient = _httpClientFactory.CreateClient("parallel");
            var json = JsonSerializer.Serialize(customer);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var url = "/api/Customers/Bradesco";

            using (var httpResponse = await _httpClient.PostAsync(url, data))
            {
                //var result = await httpResponse.Content.ReadAsStringAsync();
                if (httpResponse.IsSuccessStatusCode)
                    _logger.LogInformation($"{httpResponse.IsSuccessStatusCode}");
                else
                    _logger.LogError($"{httpResponse.IsSuccessStatusCode}");
            }
        }
        catch (Exception ex)
        {
            _logger.LogError($"Ocorreu o seguinte erro: {ex.Message}");
        }
    }

    private async Task SendCustomersItau(Customers customer)
    {

        //try
        //{
        //var options = new JsonSerializerOptions
        //{
        //PropertyNameCaseInsensitive = true
        ////WriteIndented = true,
        ////DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
        ////AllowTrailingCommas = true,
        ////MaxDepth = 10,
        ////ReferenceHandler = ReferenceHandler.Preserve,
        ////Converters = { new JsonStringEnumConverter() }
        //};

        var _httpClient = _httpClientFactory.CreateClient("parallel");
        var _content = new StringContent(JsonSerializer.Serialize(customer), Encoding.UTF8, "application/json");
        var _url = "api/Customers/Itau";
        using var _httpResponse = await _httpClient.PostAsync(_url, _content);
        var _result = await _httpResponse.Content.ReadAsStringAsync();

        if (_httpResponse.IsSuccessStatusCode)
        {
            _logger.LogInformation($"Sucesso");
        }
        else
        {
            ResponseHttpDto? _response = JsonSerializer.Deserialize<ResponseHttpDto>(_result, _options);
            if (_httpResponse.StatusCode == HttpStatusCode.InternalServerError)
                _logger.LogError($"Sucess: {_response?.success} - Message: {_response?.resultMessage} - StatusCode: {_response?.status} | Title: {_response?.title} | Type: {_response?.type} | TaceId: {_response?.traceId}");
            else
                _logger.LogWarning($"Sucess: {_response?.success} - Message: {_response?.resultMessage} - StatusCode: {_response?.status} | Title: {_response?.title} | Type: {_response?.type} | TaceId: {_response?.traceId}");
        }
    }

    async void LerArquivoParaMultiplosChannels()
    {
        try
        {
            string caminhoPlanilha = @"c:\Adriano\Arquivos\Clientes_Itau.csv";

            string linha = "";
            string[] linhaseparada = null;
            int total = 0;
            StreamReader reader = new StreamReader(caminhoPlanilha, Encoding.UTF8, true);

            while (true)
            {
                linha = reader.ReadLine();
                if (linha == null) break;
                if (total > 0)
                {
                    linhaseparada = linha.Split(',');
                    Customers customer = new Customers();
                    customer.CodigoHtml = linhaseparada[0].Replace("\"", "");
                    customer.CodigoInterno = linhaseparada[1].Replace("\"", "");
                    customer.Address.UfParametro = linhaseparada[2].Replace("\"", "");
                    customer.CnpjParametro = linhaseparada[3].Replace("\"", "");
                    customer.CnpjConsultado = linhaseparada[4].Replace("\"", "");
                    customer.NumeroInscricao = linhaseparada[5].Replace("\"", "");
                    customer.NomeEmpresarial = linhaseparada[6].Replace("\"", "");
                    customer.Address.Logradouro = linhaseparada[7].Replace("\"", "");
                    customer.Address.Numero = linhaseparada[8].Replace("\"", "");
                    customer.Address.Complemento = linhaseparada[9].Replace("\"", "");
                    customer.Address.CEP = linhaseparada[10].Replace("\"", "");
                    customer.Address.Bairro = linhaseparada[11].Replace("\"", "");
                    customer.Address.Municipio = linhaseparada[12].Replace("\"", "");
                    customer.Address.UF = linhaseparada[13].Replace("\"", "");
                    customer.Address.CodigoIbge = linhaseparada[14].Replace("\"", "");
                    customer.InscricaoEstadual = linhaseparada[15].Replace("\"", "");

                    await SendParaMultiplosChannels(customer);
                    //Thread.Sleep(1000);
                }
                total++;
            }

            Console.WriteLine($"Thread NUBANK CSV processou {total - 1} registros e foi executada em {Thread.CurrentThread.ManagedThreadId} ms.");

        }
        catch (Exception ex)
        {
            _logger.LogError($"Ocorreu o seguinte erro: {ex.Message}");
        }

    }

    async void LerArquivoNubank()
    {
        try
        {
            string caminhoPlanilha = @"c:\Adriano\Arquivos\Clientes_Nubank.csv";
            string linha = "";
            string[] linhaseparada = null;
            int total = 0;
            StreamReader reader = new StreamReader(caminhoPlanilha, Encoding.UTF8, true);

            while (true)
            {
                linha = reader.ReadLine();
                if (linha == null) break;
                if (total > 0)
                {
                    linhaseparada = linha.Split(',');
                    Customers customer = new Customers();
                    customer.CodigoHtml = linhaseparada[0].Replace("\"", "");
                    customer.CodigoInterno = linhaseparada[1].Replace("\"", "");
                    customer.Address.UfParametro = linhaseparada[2].Replace("\"", "");
                    customer.CnpjParametro = linhaseparada[3].Replace("\"", "");
                    customer.CnpjConsultado = linhaseparada[4].Replace("\"", "");
                    customer.NumeroInscricao = linhaseparada[5].Replace("\"", "");
                    customer.NomeEmpresarial = linhaseparada[6].Replace("\"", "");
                    customer.Address.Logradouro = linhaseparada[7].Replace("\"", "");
                    customer.Address.Numero = linhaseparada[8].Replace("\"", "");
                    customer.Address.Complemento = linhaseparada[9].Replace("\"", "");
                    customer.Address.CEP = linhaseparada[10].Replace("\"", "");
                    customer.Address.Bairro = linhaseparada[11].Replace("\"", "");
                    customer.Address.Municipio = linhaseparada[12].Replace("\"", "");
                    customer.Address.UF = linhaseparada[13].Replace("\"", "");
                    customer.Address.CodigoIbge = linhaseparada[14].Replace("\"", "");
                    customer.InscricaoEstadual = linhaseparada[15].Replace("\"", "");
                    customer.ClientName = "Nubank";
                    Thread.Sleep(1300);
                    await SendCustomersNubank(customer);
                }
                total++;
            }
            _logger.LogInformation($"Nubank - Thread Nubank processou {total} registros e foi executada em {Thread.CurrentThread.ManagedThreadId} ms.");
        }
        catch (Exception ex)
        {
            _logger.LogError($"Nubank - Ocorreu o seguinte erro: {ex.Message}");
        }

    }

    async void LerArquivoItau()
    {
        try
        {
            string caminhoPlanilha = @"c:\Adriano\Arquivos\Clientes_Itau.xlsx";
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var arquivoExcel = new ExcelPackage(new FileInfo(caminhoPlanilha));
            ExcelWorksheet PlanilhaBaseBaan = arquivoExcel.Workbook.Worksheets["ClientesItau"];
            int rows = PlanilhaBaseBaan.Dimension.Rows;
            int total = 0;

            Customers customer = new Customers();
            for (int i = 2; i <= rows; i++)
            {
                customer.CodigoHtml = PlanilhaBaseBaan.Cells[i, 1].Value == null ? null : PlanilhaBaseBaan.Cells[i, 1].Value.ToString();
                customer.CodigoInterno = PlanilhaBaseBaan.Cells[i, 2].Value == null ? null : PlanilhaBaseBaan.Cells[i, 2].Value.ToString();
                customer.Address.UfParametro = PlanilhaBaseBaan.Cells[i, 3].Value == null ? null : PlanilhaBaseBaan.Cells[i, 3].Value.ToString();
                customer.CnpjParametro = PlanilhaBaseBaan.Cells[i, 4].Value == null ? null : PlanilhaBaseBaan.Cells[i, 4].Value.ToString();
                customer.CnpjConsultado = PlanilhaBaseBaan.Cells[i, 5].Value == null ? null : PlanilhaBaseBaan.Cells[i, 5].Value.ToString();
                customer.NumeroInscricao = PlanilhaBaseBaan.Cells[i, 6].Value == null ? null : PlanilhaBaseBaan.Cells[i, 6].Value.ToString();
                customer.NomeEmpresarial = PlanilhaBaseBaan.Cells[i, 7].Value == null ? null : PlanilhaBaseBaan.Cells[i, 7].Value.ToString();
                customer.Address.Logradouro = PlanilhaBaseBaan.Cells[i, 8].Value == null ? null : PlanilhaBaseBaan.Cells[i, 8].Value.ToString();
                customer.Address.Numero = PlanilhaBaseBaan.Cells[i, 9].Value == null ? null : PlanilhaBaseBaan.Cells[i, 9].Value.ToString();
                customer.Address.Complemento = PlanilhaBaseBaan.Cells[i, 10].Value == null ? null : PlanilhaBaseBaan.Cells[i, 10].Value.ToString();
                customer.Address.CEP = PlanilhaBaseBaan.Cells[i, 11].Value == null ? null : PlanilhaBaseBaan.Cells[i, 11].Value.ToString();
                customer.Address.Bairro = PlanilhaBaseBaan.Cells[i, 12].Value == null ? null : PlanilhaBaseBaan.Cells[i, 12].Value.ToString();
                customer.Address.Municipio = PlanilhaBaseBaan.Cells[i, 13].Value == null ? null : PlanilhaBaseBaan.Cells[i, 13].Value.ToString();
                customer.Address.UF = PlanilhaBaseBaan.Cells[i, 14].Value == null ? null : PlanilhaBaseBaan.Cells[i, 14].Value.ToString();
                customer.Address.CodigoIbge = PlanilhaBaseBaan.Cells[i, 15].Value == null ? null : PlanilhaBaseBaan.Cells[i, 15].Value.ToString();
                customer.InscricaoEstadual = PlanilhaBaseBaan.Cells[i, 16].Value == null ? null : PlanilhaBaseBaan.Cells[i, 16].Value.ToString();
                customer.ClientName = "Itau";
                await SendCustomersItau(customer);
                Thread.Sleep(120000);
                total++;
            }
            _logger.LogInformation($"Ita� - Thread ITA� processou {total} registros e foi executada em {Thread.CurrentThread.ManagedThreadId} ms.");
        }
        catch (Exception ex)
        {
            _logger.LogError($"Ita� - Ocorreu o seguinte erro: {ex.Message}");
        }

    }

    async void LerArquivoBradesco()
    {
        try
        {
            string caminhoPlanilha = @"c:\Adriano\Arquivos\Clientes_Bradesco.xlsx";
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var arquivoExcel = new ExcelPackage(new FileInfo(caminhoPlanilha));
            ExcelWorksheet PlanilhaBaseBaan = arquivoExcel.Workbook.Worksheets["ClientesBradesco"];
            int rows = PlanilhaBaseBaan.Dimension.Rows;
            int total = 0;

            Customers customer = new Customers();
            for (int i = 2; i <= rows; i++)
            {
                customer.CodigoHtml = PlanilhaBaseBaan.Cells[i, 1].Value == null ? null : PlanilhaBaseBaan.Cells[i, 1].Value.ToString();
                customer.CodigoInterno = PlanilhaBaseBaan.Cells[i, 2].Value == null ? null : PlanilhaBaseBaan.Cells[i, 2].Value.ToString();
                customer.Address.UfParametro = PlanilhaBaseBaan.Cells[i, 3].Value == null ? null : PlanilhaBaseBaan.Cells[i, 3].Value.ToString();
                customer.CnpjParametro = PlanilhaBaseBaan.Cells[i, 4].Value == null ? null : PlanilhaBaseBaan.Cells[i, 4].Value.ToString();
                customer.CnpjConsultado = PlanilhaBaseBaan.Cells[i, 5].Value == null ? null : PlanilhaBaseBaan.Cells[i, 5].Value.ToString();
                customer.NumeroInscricao = PlanilhaBaseBaan.Cells[i, 6].Value == null ? null : PlanilhaBaseBaan.Cells[i, 6].Value.ToString();
                customer.NomeEmpresarial = PlanilhaBaseBaan.Cells[i, 7].Value == null ? null : PlanilhaBaseBaan.Cells[i, 7].Value.ToString();
                customer.Address.Logradouro = PlanilhaBaseBaan.Cells[i, 8].Value == null ? null : PlanilhaBaseBaan.Cells[i, 8].Value.ToString();
                customer.Address.Numero = PlanilhaBaseBaan.Cells[i, 9].Value == null ? null : PlanilhaBaseBaan.Cells[i, 9].Value.ToString();
                customer.Address.Complemento = PlanilhaBaseBaan.Cells[i, 10].Value == null ? null : PlanilhaBaseBaan.Cells[i, 10].Value.ToString();
                customer.Address.CEP = PlanilhaBaseBaan.Cells[i, 11].Value == null ? null : PlanilhaBaseBaan.Cells[i, 11].Value.ToString();
                customer.Address.Bairro = PlanilhaBaseBaan.Cells[i, 12].Value == null ? null : PlanilhaBaseBaan.Cells[i, 12].Value.ToString();
                customer.Address.Municipio = PlanilhaBaseBaan.Cells[i, 13].Value == null ? null : PlanilhaBaseBaan.Cells[i, 13].Value.ToString();
                customer.Address.UF = PlanilhaBaseBaan.Cells[i, 14].Value == null ? null : PlanilhaBaseBaan.Cells[i, 14].Value.ToString();
                customer.Address.CodigoIbge = PlanilhaBaseBaan.Cells[i, 15].Value == null ? null : PlanilhaBaseBaan.Cells[i, 15].Value.ToString();
                customer.InscricaoEstadual = PlanilhaBaseBaan.Cells[i, 16].Value == null ? null : PlanilhaBaseBaan.Cells[i, 16].Value.ToString();
                customer.ClientName = "Bradesco";
                total++;
                await SendCustomersBradesco(customer);
                Thread.Sleep(1200);
            }
            _logger.LogInformation($"Bradesco - Thread Bradesco processou {total} registros e foi executada em {Thread.CurrentThread.ManagedThreadId} ms.");
        }
        catch (Exception ex)
        {
            _logger.LogError($"Bradesco - Ocorreu o seguinte erro: {ex.Message}");
        }
    }

    async void LerArquivoSantander()
    {
        try
        {
            string caminhoPlanilha = @"c:\Adriano\Arquivos\Clientes_Santander.xlsx";
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var arquivoExcel = new ExcelPackage(new FileInfo(caminhoPlanilha));
            ExcelWorksheet PlanilhaBaseBaan = arquivoExcel.Workbook.Worksheets["ClientesSantander"];
            int rows = PlanilhaBaseBaan.Dimension.Rows;
            int total = 0;

            Customers customer = new Customers();
            for (int i = 2; i <= rows; i++)
            {
                customer.CodigoHtml = PlanilhaBaseBaan.Cells[i, 1].Value == null ? null : PlanilhaBaseBaan.Cells[i, 1].Value.ToString();
                customer.CodigoInterno = PlanilhaBaseBaan.Cells[i, 2].Value == null ? null : PlanilhaBaseBaan.Cells[i, 2].Value.ToString();
                customer.Address.UfParametro = PlanilhaBaseBaan.Cells[i, 3].Value == null ? null : PlanilhaBaseBaan.Cells[i, 3].Value.ToString();
                customer.CnpjParametro = PlanilhaBaseBaan.Cells[i, 4].Value == null ? null : PlanilhaBaseBaan.Cells[i, 4].Value.ToString();
                customer.CnpjConsultado = PlanilhaBaseBaan.Cells[i, 5].Value == null ? null : PlanilhaBaseBaan.Cells[i, 5].Value.ToString();
                customer.NumeroInscricao = PlanilhaBaseBaan.Cells[i, 6].Value == null ? null : PlanilhaBaseBaan.Cells[i, 6].Value.ToString();
                customer.NomeEmpresarial = PlanilhaBaseBaan.Cells[i, 7].Value == null ? null : PlanilhaBaseBaan.Cells[i, 7].Value.ToString();
                customer.Address.Logradouro = PlanilhaBaseBaan.Cells[i, 8].Value == null ? null : PlanilhaBaseBaan.Cells[i, 8].Value.ToString();
                customer.Address.Numero = PlanilhaBaseBaan.Cells[i, 9].Value == null ? null : PlanilhaBaseBaan.Cells[i, 9].Value.ToString();
                customer.Address.Complemento = PlanilhaBaseBaan.Cells[i, 10].Value == null ? null : PlanilhaBaseBaan.Cells[i, 10].Value.ToString();
                customer.Address.CEP = PlanilhaBaseBaan.Cells[i, 11].Value == null ? null : PlanilhaBaseBaan.Cells[i, 11].Value.ToString();
                customer.Address.Bairro = PlanilhaBaseBaan.Cells[i, 12].Value == null ? null : PlanilhaBaseBaan.Cells[i, 12].Value.ToString();
                customer.Address.Municipio = PlanilhaBaseBaan.Cells[i, 13].Value == null ? null : PlanilhaBaseBaan.Cells[i, 13].Value.ToString();
                customer.Address.UF = PlanilhaBaseBaan.Cells[i, 14].Value == null ? null : PlanilhaBaseBaan.Cells[i, 14].Value.ToString();
                customer.Address.CodigoIbge = PlanilhaBaseBaan.Cells[i, 15].Value == null ? null : PlanilhaBaseBaan.Cells[i, 15].Value.ToString();
                customer.InscricaoEstadual = PlanilhaBaseBaan.Cells[i, 16].Value == null ? null : PlanilhaBaseBaan.Cells[i, 16].Value.ToString();
                customer.ClientName = "Santander";
                total++;
                await SendCustomersSantander(customer);
                Thread.Sleep(1000);
            }
            _logger.LogInformation($"Santander - Thread Santander processou {total} registros e foi executada em {Thread.CurrentThread.ManagedThreadId} ms.");
        }
        catch (Exception ex)
        {
            _logger.LogError($"Santander - Ocorreu o seguinte erro: {ex.Message}");
        }
    }


}
