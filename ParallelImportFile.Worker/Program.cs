using ParallelImportFile.Worker;

var builder = Host.CreateApplicationBuilder(args);
builder.Services.AddSingleton<Worker>();

var host = builder.Build();
host.Run();
