﻿using RabbitMQ.Client;
using System.Text;
using System.Text.Json;

namespace ParallelImportFile.worker.RabbitMQ
{
    public class RabbitMQProducer : IRabbitMQProducer
    {
        public RabbitMQProducer()
        {
        }

        public void SendMessageNubank<T>(T message)
        {
            var factory = new ConnectionFactory { HostName = "localhost" };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare(queue: "clientes-nubank",
                                durable: false,
                                exclusive: false,
                                autoDelete: false,
                                arguments: null);

            var json = JsonSerializer.Serialize(message);
            var body = Encoding.UTF8.GetBytes(json);

            channel.BasicPublish(exchange: string.Empty,
                                routingKey: "clientes-nubank",
                                basicProperties: null,
                                body: body);
        }

        public void SendMessageItau<T>(T message)
        {
            var factory = new ConnectionFactory { HostName = "localhost" };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare(queue: "clientes-itau",
                                durable: false,
                                exclusive: false,
                                autoDelete: false,
                                arguments: null);

            var json = JsonSerializer.Serialize(message);
            var body = Encoding.UTF8.GetBytes(json);

            channel.BasicPublish(exchange: string.Empty,
                                routingKey: "clientes-itau",
                                basicProperties: null,
                                body: body);
        }

        public void SendMessageSantander<T>(T message)
        {
            var factory = new ConnectionFactory { HostName = "localhost" };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare(queue: "clientes-santander",
                                durable: false,
                                exclusive: false,
                                autoDelete: false,
                                arguments: null);

            var json = JsonSerializer.Serialize(message);
            var body = Encoding.UTF8.GetBytes(json);

            channel.BasicPublish(exchange: string.Empty,
                                routingKey: "clientes-santander",
                                basicProperties: null,
                                body: body);
        }

        public void SendMessageBradesco<T>(T message)
        {
            var factory = new ConnectionFactory { HostName = "localhost" };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare(queue: "clientes-bradesco",
                                durable: false,
                                exclusive: false,
                                autoDelete: false,
                                arguments: null);

            var json = JsonSerializer.Serialize(message);
            var body = Encoding.UTF8.GetBytes(json);

            channel.BasicPublish(exchange: string.Empty,
                                routingKey: "clientes-bradesco",
                                basicProperties: null,
                                body: body);
        }
    }
}
