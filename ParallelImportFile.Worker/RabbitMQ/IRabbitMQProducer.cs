﻿namespace ParallelImportFile.worker.RabbitMQ
{
    public interface IRabbitMQProducer
    {
        public void SendMessageNubank<T>(T message);
        public void SendMessageItau<T>(T message);
        public void SendMessageBradesco<T>(T message);
        public void SendMessageSantander<T>(T message);
    }
}
